#!/usr/bin/env python3
# -*- coding:utf-8 -*-
###
# Copyright (c) 2022 Haofan Zheng
# Use of this source code is governed by an MIT-style
# license that can be found in the LICENSE file or at
# https://opensource.org/licenses/MIT.
###


import time

from typing import Callable


class RepeatTaskSafeGuard(object):

	def __init__(self, safeIntervalSec: float) -> None:
		super(RepeatTaskSafeGuard, self).__init__()

		self.safeInterval = safeIntervalSec
		self.lastRunTime = 0

	def Run(self, target: Callable, args: tuple = ()) -> bool:
		now = time.time()

		if (now - self.lastRunTime) >= self.safeInterval:
			self.lastRunTime = now
			target(*args)
			return True

		return False

	def SetLastRunTimeToNow(self) -> None:
		self.lastRunTime = time.time()
