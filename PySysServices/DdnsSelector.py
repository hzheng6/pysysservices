#!/usr/bin/env python3
# -*- coding:utf-8 -*-
###
# Copyright (c) 2022 Haofan Zheng
# Use of this source code is governed by an MIT-style
# license that can be found in the LICENSE file or at
# https://opensource.org/licenses/MIT.
###


from typing import Dict, List, Tuple


class DNSRecSelectorBase(object):
	def __init__(self) -> None:
		super(DNSRecSelectorBase, self).__init__()

	def Select(self, rawData: dict) -> List[str]:
		raise NotImplementedError('Select method not implemented')

	def GetType(self) -> str:
		raise NotImplementedError('GetType method not implemented')

	def IsEqual(self, a: List[str], b: List[str]) -> bool:
		return set(a) == set(b)

	def GenStrategy(
		self,
		currList: List[str],
		newList: List[str]
	) -> Tuple[List[str], List[str]]:
		removeList = []
		addList = []
		for item in currList:
			if item not in newList:
				removeList.append(item)

		for item in newList:
			if item not in currList:
				addList.append(item)

		return (removeList, addList)


class DNSRecSelectorIPv4(DNSRecSelectorBase):
	def __init__(self) -> None:
		super(DNSRecSelectorIPv4, self).__init__()

	def Select(self, rawData: dict) -> List[str]:
		if 'ipv4' in rawData:
			return [ x['addr'] for x in rawData['ipv4'] ]
		else:
			return []

	def GetType(self) -> str:
		return 'A'


SELECTOR_MAP: Dict[str, DNSRecSelectorBase] = {
	'IPv4': DNSRecSelectorIPv4,
}


def ConstructSelectors(selNames: List[str]) -> List[DNSRecSelectorBase]:
	return [ SELECTOR_MAP[x]() for x in selNames ]
