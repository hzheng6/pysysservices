#!/usr/bin/env python3
# -*- coding:utf-8 -*-
###
# Copyright (c) 2022 Haofan Zheng
# Use of this source code is governed by an MIT-style
# license that can be found in the LICENSE file or at
# https://opensource.org/licenses/MIT.
###


import logging
import os
import ssl
import http.client

class HttpsSource:

	def __init__(self, srcName, globalConf):

		self.srcName = srcName
		self.isBatch = globalConf['isBatch']
		self.stateDir = globalConf['stateDir']
		self.srcStateDir = os.path.join(self.stateDir, 'src')

		self.logger = logging.getLogger(
			__name__ + '.' + HttpsSource.__name__ + '.' + self.srcName)

		if not os.path.isdir(self.srcStateDir):
			os.makedirs(self.srcStateDir)

		# check if need to get address from the user:
		addrFilePath = os.path.join(self.srcStateDir, self.srcName + '.addr')
		if self.isBatch or os.path.isfile(addrFilePath):
			with open(addrFilePath, 'r') as addrFile:
				self.addr = addrFile.read().strip()
		else:
			# We need to get address from user input
			self.addr = input(
				'Please enter address for source {}: '.format(srcName)).strip()
			# save to local state
			with open(addrFilePath, 'w') as addrFile:
				addrFile.write(self.addr)

		addrPrefix = self.addr.find('://')
		if addrPrefix >= 0:
			if not self.addr.startswith('https://'):
				raise ValueError('Invalid HTTPS address')
			else:
				self.hostName = self.addr[8:]
		else:
			self.hostName = self.addr

		slashPos = self.hostName.find('/')
		self.subaddr = '/' if slashPos < 0 else self.hostName[slashPos:]
		self.hostName = self.hostName if slashPos < 0 else self.hostName[:slashPos]

		if not self.subaddr.endswith('/'):
			self.subaddr += '/'

		self.logger.info('Initialized source host={}, subaddr={}'.format(
			self.hostName, self.subaddr))

	def FetchContent(self, srcPath: str) -> bytes:

		conn = http.client.HTTPSConnection(self.hostName)
		conn.request('GET', self.subaddr + srcPath)
		ssl.match_hostname(conn.sock.getpeercert(), self.hostName)
		resp = conn.getresponse()
		if resp.status != 200:
			conn.close()
			raise Exception(
				'Failed to get content from {} - Status: {}.'.format(
					self.addr, resp.status
				)
			)
		content = resp.read()
		conn.close()
		return content
