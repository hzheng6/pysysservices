#!/usr/bin/env python3
# -*- coding:utf-8 -*-
###
# Copyright (c) 2022 Haofan Zheng
# Use of this source code is governed by an MIT-style
# license that can be found in the LICENSE file or at
# https://opensource.org/licenses/MIT.
###


import os
import logging
import threading

from pathlib import Path


class ServiceFunction:

	def __init__(self, svcId, conf, gConf, svcStateDir):
		'''
		Initialize a ServiceFunction object.

		- Args:
		  - svcId: The ID of the service
		  - conf: Configuration of the service
		  - svcStateDir: The directory where the services should store its state;
		              if the service is set to non-repeat, a flag file will be
		              created in this directory to indicate that the service has
		              been run
		'''

		self.svcStateDir = svcStateDir
		self.svcId = svcId
		self.repeat = conf['repeat']
		self.updInterval = gConf['updInterval']
		self.svcFuncName = self.__class__.__name__

		self.logger = logging.getLogger(
			__name__ + '.' + self.__class__.__name__)

		self.repFlagFilePath = os.path.join(
			self.svcStateDir, 'ran-flag-' + self.svcId)
		self.svcType = None
		self.hasStarted = False
		self.isTerminating = False
		self.svcThread = None


	def RunService(self, globalState, globalCache):
		'''
		Override this method to implement the service's functionality.

		- Args:
		  - globalState: A dictionary that can be used to store state that is
		                 shared across all services; the state will be preserved
		                 across each runs (encoded in JSON, saved on drive)
		  - globalCache: A dictionary that can be used to store state that is
		                 shared across all services; the state will not be
		                 preserved
		'''
		pass


	def StopService(self):
		'''
		Ovveride this method to implement the service's shutdown functionality.
		This is useful for services running parallel to the main process, so
		that it will be notified when the main process is shutting down
		'''
		pass


	def _RunServiceCatchException(self, globalState, globalCache):
		try:
			self.RunService(globalState, globalCache)
		except Exception as e:
			self.logger.error(
				'Failed to run service {} with error {}'.format(
					self.svcId, e))


	def _ExecBasedOnType(self, globalState, globalCache):
		if self.svcType == 'OneShot':
			self.hasStarted = True
			self._RunServiceCatchException(globalState, globalCache)
		elif self.svcType == 'Daemon':
			self.svcThread = threading.Thread(
				target=self._RunServiceCatchException,
				args=(globalState, globalCache)
			)
			self.svcThread.start()
			self.hasStarted = True
		else:
			raise ValueError('Invalid service type: {}'.format(self.svcType))


	def Start(self, globalState, globalCache):
		if self.repeat:
			try:
				self.logger.info('Starting {}'.format(self.__str__()))
				self._ExecBasedOnType(globalState, globalCache)
			except Exception as e:
				self.logger.error(
					'Failed to run service {} with error {}'.format(
						self.svcId, e))
		elif not os.path.isfile(self.repFlagFilePath):
			try:
				self.logger.info('Starting {}'.format(self.__str__()))
				self._ExecBasedOnType(globalState, globalCache)
				Path(self.repFlagFilePath).touch()
			except Exception as e:
				self.logger.error(
					'Failed to run service {} with error {}'.format(
						self.svcId, e))


	def Shutdown(self):
		self.isTerminating = True
		if self.hasStarted:
			self.StopService()
			if self.svcThread is not None:
				self.svcThread.join()
			self.hasStarted = False


	def GetStatus(self):
		return {
			'ServiceID'       : self.svcId,
			'ServiceFunction' : self.svcFuncName,
			'ServiceType'     : self.svcType,
			'IsRepeat'        : self.repeat,
			'StateDirectory'  : self.svcStateDir,
			'HasStarted'      : self.hasStarted,
			'IsTerminating'   : self.isTerminating,
			'ServiceThread'   : None if self.svcThread is None else {
				'Name'     : self.svcThread.name,
				'Ident'    : self.svcThread.ident,
				'IsAlive'  : self.svcThread.is_alive(),
				'IsDaemon' : self.svcThread.daemon,
			},
		}


	def SvcDescriptionItems(self):
		return {
			'ServiceID'     : str(self.svcId[:8]),
			'ServiceType'   : str(self.svcType),
		}


	def __str__(self):

		return '{' + ' '.join(
			['{}: {}'.format(k, v) for k, v in
				self.SvcDescriptionItems().items()]
		) + '}'
