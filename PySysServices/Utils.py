#!/usr/bin/env python3
# -*- coding:utf-8 -*-
###
# Copyright (c) 2022 Haofan Zheng
# Use of this source code is governed by an MIT-style
# license that can be found in the LICENSE file or at
# https://opensource.org/licenses/MIT.
###


import hashlib
import os

from typing import Any, List, Union


def CalcSvcId(src: str) -> str:
	return hashlib.sha256(src.encode()).hexdigest()


def ProcOptionalConfig(config, key, default):
	if (key in config) and (config[key] is not None):
		return config[key]
	else:
		return default


def BriefText(text: str, maxLen: int):
	if len(text) > maxLen:
		return '{}...{}'.format(text[:maxLen // 2], text[-maxLen // 2:])
	else:
		return text


def AtomicWriteFile(path: str, content: Union[str, bytes], mode: str):
	tmpPath = path + '.' + os.urandom(8).hex() + '.tmp'
	with open(tmpPath, mode) as file:
		file.write(content)
	os.replace(tmpPath, path)


def FetchSubDataFromDataset(dataset: dict, path: List[str]) -> Any:
	if len(path) == 0:
		return dataset
	else:
		return FetchSubDataFromDataset(dataset[path[0]], path[1:])
