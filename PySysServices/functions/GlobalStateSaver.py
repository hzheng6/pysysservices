#!/usr/bin/env python3
# -*- coding:utf-8 -*-
###
# Copyright (c) 2022 Haofan Zheng
# Use of this source code is governed by an MIT-style
# license that can be found in the LICENSE file or at
# https://opensource.org/licenses/MIT.
###


import json
import logging
import os
import time

from .. import RepeatTaskSafeGuard
from .. import ServiceFunction
from .. import Utils


class GlobalStateSaver(ServiceFunction.ServiceFunction):

	def __init__(self, svcStateDir, config, globalConf):

		svcId = Utils.CalcSvcId(
			'{}.{}'.format(
				__name__, self.__class__.__name__))

		super(GlobalStateSaver, self).__init__(
			svcId, config, globalConf, svcStateDir)

		self.logger = logging.getLogger(
			__name__ + '.' + self.__class__.__name__)

		self.statePath = os.path.join(
			globalConf['stateDir'], 'globalStates.json')
		self.lastWroteContent = ''
		self.repSafeGuard = RepeatTaskSafeGuard.RepeatTaskSafeGuard(1)

		self.svcType = "Daemon"


	def _FilterFailingStates(self, globalState: dict) -> dict:
		# filter out the states that can't be serialized by json
		filteredState = {}
		for key, val in globalState.items():
			try:
				json.dumps({key: val})
				filteredState[key] = val
			except:
				pass
		return filteredState


	def _SafeGuardTarget(self, content):
		Utils.AtomicWriteFile(self.statePath, content, 'w')
		self.lastWroteContent = content
		self.logger.debug('Updated saved global state')


	def RunService(self, globalState, globalCache):
		while not self.isTerminating:
			filteredState = self._FilterFailingStates(globalState)
			content = json.dumps(filteredState, indent='\t')
			if content != self.lastWroteContent:
				self.repSafeGuard.Run(
					target=self._SafeGuardTarget,
					args=(content,))

			time.sleep(self.updInterval)


	def GetStatus(self):
		return {
			**super(GlobalStateSaver, self).GetStatus(),
			**{
				'statePath'      : self.statePath,
			}
		}


	def SvcDescriptionItems(self):
		return {
			**super(GlobalStateSaver, self).SvcDescriptionItems(),
			**{
				'statePath'      : self.statePath,
			}
		}
