#!/usr/bin/env python3
# -*- coding:utf-8 -*-
###
# Copyright (c) 2022 Haofan Zheng
# Use of this source code is governed by an MIT-style
# license that can be found in the LICENSE file or at
# https://opensource.org/licenses/MIT.
###


import os
import logging

from .. import ServiceFunction
from .. import Utils

class TextAppend(ServiceFunction.ServiceFunction):

	def __init__(self, svcStateDir, config, globalConf):

		path = os.path.abspath(config['path'])
		text = [x for x in config['text']]
		uid = config['uniId']

		svcId = Utils.CalcSvcId(
			'{}.{}.{}.{}.{}'.format(
				__name__, self.__class__.__name__, path, text, uid))

		super(TextAppend, self).__init__(
			svcId, config, globalConf, svcStateDir)

		self.logger = logging.getLogger(
			__name__ + '.' + self.__class__.__name__)

		self.path = path
		self.text = text
		self.uid = uid

		self.lineEnd = Utils.ProcOptionalConfig(
			config, 'lineEnd', '\n')

		self.briefDestPath = Utils.BriefText(self.path, 43)

		self.svcType = "OneShot"


	def RunService(self, globalState, globalCache):
		with open(self.path, 'a') as outFile:
			for item in self.text:
				outFile.write(item)
				outFile.write(self.lineEnd)


	def GetStatus(self):
		return {
			**super(TextAppend, self).GetStatus(),
			**{
				'SourcePath'        : self.path,
				'AppendingText'     : self.text,
			}
		}


	def SvcDescriptionItems(self):
		return {
			**super(TextAppend, self).SvcDescriptionItems(),
			**{
				'Destination'     : self.briefDestPath,
			}
		}
