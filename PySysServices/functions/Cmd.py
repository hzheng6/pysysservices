#!/usr/bin/env python3
# -*- coding:utf-8 -*-
###
# Copyright (c) 2022 Haofan Zheng
# Use of this source code is governed by an MIT-style
# license that can be found in the LICENSE file or at
# https://opensource.org/licenses/MIT.
###


import os
import sys
import logging
import getpass
import subprocess
import time

from .. import RepeatTaskSafeGuard
from .. import ServiceFunction
from .. import Utils

class Cmd(ServiceFunction.ServiceFunction):

	def __init__(self, svcStateDir, config, globalConf):

		self.uniId = config['uniId']
		svcId = Utils.CalcSvcId(
			'{}.{}.{}'.format(__name__, self.__class__.__name__, self.uniId))

		super(Cmd, self).__init__(svcId, config, globalConf, svcStateDir)

		self.logger = logging.getLogger(
			__name__ + '.' + self.__class__.__name__ + '.' + self.uniId)

		self.cmd = config['cmd']
		self.cwd = os.path.abspath(config['cwd'])
		self.svcType = config['svcType']
		self.restart = config['restart']

		self.env = Utils.ProcOptionalConfig(config, 'env', {})
		self.user = Utils.ProcOptionalConfig(
			config, 'user', getpass.getuser())
		self.isInteractive = Utils.ProcOptionalConfig(
			config, 'interactive', False)
		self.restartDelay = Utils.ProcOptionalConfig(
			config, 'restartDelay', 5)

		self.isInBatchMode = globalConf['isBatch']

		self.progOutputPath = os.path.join(
			self.svcStateDir, 'pout-' + self.svcId + '.log')
		self.progOutputPath = Utils.ProcOptionalConfig(
			config, 'progOutput', self.progOutputPath)
		self.progOutputMode = Utils.ProcOptionalConfig(
			config, 'progOutputMode', 'a')

		self.terminateTimer = Utils.ProcOptionalConfig(
			config, 'terminateTimer', None)

		if self.isInteractive and self.isInBatchMode:
			raise ValueError('Cannot run interactive command in batch mode')

		os.makedirs(self.cwd, exist_ok=True)

		self.proc = None
		self.procRet = None
		self.repSafeGuard = RepeatTaskSafeGuard.RepeatTaskSafeGuard(
			self.restartDelay)


	def _RunBasedOnInteract(self):
		if self.isInteractive:
			self.proc = subprocess.Popen(self.cmd, cwd=self.cwd,
				stdin=sys.stdin, stdout=sys.stdout, stderr=sys.stderr,
				env={**os.environ,**self.env})
		else:
			with open(self.progOutputPath, self.progOutputMode) as logFile:
				self.proc = subprocess.Popen(self.cmd, cwd=self.cwd,
					stdout=logFile, stderr=logFile,
					env={**os.environ,**self.env})


	def _RunWithDaemonGuard(self):
		while not self.isTerminating:
			if self.proc is not None:
				# The process is already running
				if (
					(self.terminateTimer is not None) and
					(time.time() >= (self.repSafeGuard.lastRunTime + self.terminateTimer))
				):
					# time to terminate the process
					self.proc.terminate()
					self.proc.wait()

				self.procRet = self.proc.poll()
				if self.procRet is not None:
					# The process has exited
					self.logger.info(
						'Process exited with code {}'.format(self.procRet))
					self.proc = None
				else:
					self.repSafeGuard.SetLastRunTimeToNow()
			else:
				# The process is not running
				if (self.procRet is None) or (self.restart == 'Always'):
					# The process has not exited or we want to restart it
					self.repSafeGuard.Run(
						target=self._RunBasedOnInteract)
				else:
					# The process has exited and we don't want to restart it
					return None

			time.sleep(self.updInterval)


	def RunService(self, globalState, globalCache):
		if self.svcType == 'OneShot':
			self.repSafeGuard.Run(
				target=self._RunBasedOnInteract)
			self.proc.wait()
			self.proc = None
		elif self.svcType == 'Daemon':
			self._RunWithDaemonGuard()
		else:
			raise ValueError('Invalid service type: {}'.format(self.svcType))


	def StopService(self):
		if self.proc is not None:
			self.proc.terminate()
			self.proc.wait()


	def GetStatus(self):
		return {
			**super(Cmd, self).GetStatus(),
			**{
				'Command'      : ' '.join(self.cmd),
				'WorkingDir'   : self.cwd,
			}
		}


	def SvcDescriptionItems(self):
		return {
			**super(Cmd, self).SvcDescriptionItems(),
			**{
				'UniqueID'     : str(self.uniId),
			}
		}
