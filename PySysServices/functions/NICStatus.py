#!/usr/bin/env python3
# -*- coding:utf-8 -*-
###
# Copyright (c) 2022 Haofan Zheng
# Use of this source code is governed by an MIT-style
# license that can be found in the LICENSE file or at
# https://opensource.org/licenses/MIT.
###


import logging
import time

import netifaces as ni # python3 -m pip install netifaces

from .. import RepeatTaskSafeGuard
from .. import ServiceFunction
from .. import Utils


class NICStatus(ServiceFunction.ServiceFunction):

	def __init__(self, svcStateDir, config, globalConf):

		nicName = config['nic']

		svcId = Utils.CalcSvcId(
			'{}.{}.{}'.format(
				__name__, self.__class__.__name__, nicName))

		super(NICStatus, self).__init__(
			svcId, config, globalConf, svcStateDir)

		self.logger = logging.getLogger(
			__name__ + '.' + self.__class__.__name__ + '.' + nicName)

		self.nicName = nicName
		self.interval = Utils.ProcOptionalConfig(
			config, 'interval', 1)

		self.nicStatus = {}
		self.repSafeGuard = RepeatTaskSafeGuard.RepeatTaskSafeGuard(
			self.interval)

		self.svcType = "Daemon"


	def _SafeGuardTarget(self, globalState, globalCache):
		self.nicStatus = {}
		if self.nicName in ni.interfaces():
			addresses = ni.ifaddresses(self.nicName)

			# assuming mac address is always there and is only one address
			self.nicStatus['mac'] = addresses[ni.AF_LINK][0]['addr']

			if ni.AF_INET in addresses:
				self.nicStatus['ipv4'] = addresses[ni.AF_INET]
			else:
				self.nicStatus['ipv4'] = []

			if ni.AF_INET6 in addresses:
				self.nicStatus['ipv6'] = addresses[ni.AF_INET6]
			else:
				self.nicStatus['ipv6'] = []

		globalState['NICStatus'][self.nicName] = self.nicStatus
		globalCache['NICStatus'][self.nicName] = self.nicStatus


	def RunService(self, globalState, globalCache):
		if 'NICStatus' not in globalState:
			globalState['NICStatus'] = {}
		if 'NICStatus' not in globalCache:
			globalCache['NICStatus'] = {}

		while not self.isTerminating:
			self.repSafeGuard.Run(
				target=self._SafeGuardTarget,
				args=(globalState, globalCache))

			time.sleep(self.updInterval)


	def GetStatus(self):
		return {
			**super(NICStatus, self).GetStatus(),
			**{
				'NICName'      : self.nicName,
				'NICStatus'    : self.nicStatus,
			}
		}


	def SvcDescriptionItems(self):
		return {
			**super(NICStatus, self).SvcDescriptionItems(),
			**{
				'NICName'      : self.nicName,
			}
		}
