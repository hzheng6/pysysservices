#!/usr/bin/env python3
# -*- coding:utf-8 -*-
###
# Copyright (c) 2022 Haofan Zheng
# Use of this source code is governed by an MIT-style
# license that can be found in the LICENSE file or at
# https://opensource.org/licenses/MIT.
###


import os
import logging
import time

from ..DHomeKeyManager.DHomeKeyManager.utils import CheckCertExpire

from ..DHomeHttpsServer.Client import(
	Request,
	SSLContext,
)

from .. import RepeatTaskSafeGuard
from .. import ServiceFunction
from .. import Utils


class DHomeCertRenew(ServiceFunction.ServiceFunction):

	def __init__(self, svcStateDir, config, globalConf):

		uid = config['uniId']

		svcId = Utils.CalcSvcId(
			'{}.{}.{}'.format(
				__name__,
				self.__class__.__name__,
				uid
			)
		)

		super(DHomeCertRenew, self).__init__(
			svcId, config, globalConf, svcStateDir)

		self.logger = logging.getLogger(
			__name__ + '.' + self.__class__.__name__)

		self.uid        = config['uniId']
		self.kmsSvrAddr = config['kmsSvrAddr']
		self.kmsSvrPort = config['kmsSvrPort']
		self.kmsSvrCA   = config['kmsSvrCA']
		self.inPrivKey  = config['inPrivKey']
		self.inCert     = config['inCert']
		self.expireIn   = config['expireIn']
		self.chkInterval= config['chkInterval']

		self.outCert    = Utils.ProcOptionalConfig(
			config, 'outCert', self.inCert)

		self.briefInCert  = Utils.BriefText(
			self.inCert, 43)
		self.briefOutCert = Utils.BriefText(
			self.outCert, 43)

		if not os.path.isfile(self.inPrivKey):
			raise RuntimeError('Private key file not found')
		if not os.path.isfile(self.inCert):
			raise RuntimeError('Certificate file not found')
		if not os.path.isfile(self.kmsSvrCA):
			raise RuntimeError('KMS server CA file not found')

		self.repSafeGuard = RepeatTaskSafeGuard.RepeatTaskSafeGuard(
			self.chkInterval
		)

		self.svcType = "Daemon"


	def _SafeGuardTarget(self):
		if CheckCertExpire(
			certFile=self.inCert,
			thresholdDays=self.expireIn,
		):
			self.logger.info(
				'Certificate {} is about to expire, ' +
				'renewing...'.format(self.briefInCert)
			)

			cltTlsCtx = SSLContext.GetAuthClientContext(
				clientCertPath = self.inCert,
				clientKeyPath  = self.inPrivKey,
				caCertPath     = self.kmsSvrCA
			)

			respRaw, respType, respParsed = Request.GetRequest(
				sslContext=cltTlsCtx,
				host=self.kmsSvrAddr,
				port=self.kmsSvrPort,
				uri='/renew_cert/',
			)
			if respType != 'json':
				raise TypeError('Unexpected response type {}'.format(respType))

			with open(self.outCert + '.tmp', 'wb') as f:
				f.write(respParsed['certPem'].encode())
				for cert in respParsed['caPems']:
					f.write(cert.encode())
			os.replace(self.outCert + '.tmp', self.outCert)

			self.logger.info(
				'Certificate {} renewed.'.format(self.briefOutCert)
			)


	def RunService(self, globalState, globalCache):
		while not self.isTerminating:

			try:
				self.repSafeGuard.Run(target=self._SafeGuardTarget)
			except Exception as e:
				self.logger.error(
					'Failed to renew certificate ' +
					'with error: {}'.format(e)
				)

			time.sleep(self.updInterval)


	def GetStatus(self):
		return {
			**super(DHomeCertRenew, self).GetStatus(),
			**{
				'InputCert'     : self.inCert,
				'OutputCert'    : self.outCert,
			}
		}


	def SvcDescriptionItems(self):
		return {
			**super(DHomeCertRenew, self).SvcDescriptionItems(),
			**{
				'InputCert'     : self.briefInCert,
				'OutputCert'    : self.briefOutCert,
			}
		}
