#!/usr/bin/env python3
# -*- coding:utf-8 -*-
###
# Copyright (c) 2022 Haofan Zheng
# Use of this source code is governed by an MIT-style
# license that can be found in the LICENSE file or at
# https://opensource.org/licenses/MIT.
###


import logging
import subprocess
import sys
import time

from .. import RepeatTaskSafeGuard
from .. import ServiceFunction
from .. import Utils


class NetDevResetter(ServiceFunction.ServiceFunction):

	def __init__(self, svcStateDir, config, globalConf):

		nicName = config['nic']

		svcId = Utils.CalcSvcId(
			'{}.{}.{}'.format(
				__name__, self.__class__.__name__, nicName)
		)

		super(NetDevResetter, self).__init__(
			svcId, config, globalConf, svcStateDir)

		self.logger = logging.getLogger(
			__name__ + '.' + self.__class__.__name__ + '.' + nicName)

		self.nicName       = config['nic']
		self.pingTarget    = config['pingTarget']
		self.pingTimeout   = Utils.ProcOptionalConfig(config, 'pingTimeout', 1)
		self.pingRetry     = Utils.ProcOptionalConfig(config, 'pingRetry', 3)
		self.checkInterval = Utils.ProcOptionalConfig(
			config, 'checkInterval', 2.0
		)
		self.resetInterval = Utils.ProcOptionalConfig(
			config, 'resetInterval', 120.0
		)
		self.upDownInterval = Utils.ProcOptionalConfig(
			config, 'resetInterval', 2.0
		)

		self.isNicOk = True
		self.resetCount = 0

		self.checkSafeGuard = RepeatTaskSafeGuard.RepeatTaskSafeGuard(
			self.checkInterval
		)
		self.resetSafeGuard = RepeatTaskSafeGuard.RepeatTaskSafeGuard(
			self.resetInterval
		)

		self.svcType = "Daemon"


	def _ResetSafeGuardTarget(self) -> None:
		self.logger.info('Resetting NIC device {}'.format(self.nicName))
		downCmd = [ 'ip', 'link', 'set', 'dev', self.nicName, 'down', ]
		upCmd   = [ 'ip', 'link', 'set', 'dev', self.nicName, 'up', ]

		proc1 = subprocess.Popen(downCmd,
			stdout=sys.stdout, stderr=sys.stderr)
		proc1.wait()

		time.sleep(self.upDownInterval)

		proc2 = subprocess.Popen(upCmd,
			stdout=sys.stdout, stderr=sys.stderr)
		proc2.wait()

		self.resetCount += 1
		self.logger.info(
			'Finished resetting with return code ({}, {})'.format(
				proc1.returncode,
				proc2.returncode
			)
		)


	def _CheckSafeGuardTarget(self) -> None:
		pingCmd = [
			'ping',
			'-c', '1',
			'-W', str(self.pingTimeout),
			'-w', str(self.pingTimeout),
			self.pingTarget,
		]

		retryLeft = self.pingRetry
		while retryLeft > 0:
			proc = subprocess.Popen(pingCmd,
				stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL
			)
			proc.wait()

			if proc.returncode == 0:
				if not self.isNicOk:
					# status changed - it was down, now it's up
					self.logger.info(
						'Network device {} is up'.format(self.nicName)
					)
				self.isNicOk = True
				return

			retryLeft -= 1

		# all retries failed
		if self.isNicOk:
			# status changed - it was OK but now it's not
			self.logger.info(
				'Network device {} is down'.format(self.nicName)
			)
		self.isNicOk = False


	def RunService(self, globalState, globalCache):
		while not self.isTerminating:
			self.checkSafeGuard.Run(target=self._CheckSafeGuardTarget)

			if not self.isNicOk:
				self.resetSafeGuard.Run(target=self._ResetSafeGuardTarget)


			time.sleep(self.updInterval)


	def GetStatus(self):
		return {
			**super(NetDevResetter, self).GetStatus(),
			**{
				'NICName'      : self.nicName,
				'IsNICOk'      : self.isNicOk,
				'ResetCount'   : self.resetCount,
			}
		}


	def SvcDescriptionItems(self):
		return {
			**super(NetDevResetter, self).SvcDescriptionItems(),
			**{
				'NICName'      : self.nicName,
				'PingTarget'   : self.pingTarget,
			}
		}
