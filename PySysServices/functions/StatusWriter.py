#!/usr/bin/env python3
# -*- coding:utf-8 -*-
###
# Copyright (c) 2022 Haofan Zheng
# Use of this source code is governed by an MIT-style
# license that can be found in the LICENSE file or at
# https://opensource.org/licenses/MIT.
###


import json
import logging
import os
import time

from typing import List

from .. import RepeatTaskSafeGuard
from .. import ServiceFunction
from .. import Utils


def _CanDumpStatusItem(item: dict):
	try:
		json.dumps(item)
		return True
	except:
		return False


class StatusWriter(ServiceFunction.ServiceFunction):

	def __init__(self, svcStateDir, config, globalConf):

		svcId = Utils.CalcSvcId(
			'{}.{}'.format(
				__name__, self.__class__.__name__))

		super(StatusWriter, self).__init__(
			svcId, config, globalConf, svcStateDir)

		self.logger = logging.getLogger(
			__name__ + '.' + self.__class__.__name__)

		self.filePath = os.path.join(
			globalConf['stateDir'], 'status.json')
		self.lastWroteContent = ''
		self.repSafeGuard = RepeatTaskSafeGuard.RepeatTaskSafeGuard(1)
		self.services = []

		self.svcType = "Daemon"


	def SetServices(self, services: List[ServiceFunction.ServiceFunction]):
		self.services = [ x for x in services ]


	def _FilterFailingStatus(self, statuses: list) -> list:
		# filter out the status that can't be serialized by json
		return [
			x for x in statuses
			if _CanDumpStatusItem(x)
		]


	def _CollectStatus(
		self,
		svcs: List[ServiceFunction.ServiceFunction]) -> list:

		return [ svc.GetStatus() for svc in svcs ]


	def _SafeGuardTarget(self, content):
		Utils.AtomicWriteFile(self.filePath, content, 'w')
		self.lastWroteContent = content
		self.logger.debug('Updated status file')


	def RunService(self, globalState, globalCache):
		while not self.isTerminating:
			statuses = self._CollectStatus(self.services)
			statuses = self._FilterFailingStatus(statuses)
			content = json.dumps(statuses, indent='\t')
			if content != self.lastWroteContent:
				self.repSafeGuard.Run(
					target=self._SafeGuardTarget,
					args=(content,))

			time.sleep(self.updInterval)


	def StopService(self):
		self.services = []


	def GetStatus(self):
		return {
			**super(StatusWriter, self).GetStatus(),
			**{
				'NumOfServices'      : len(self.services),
			}
		}


	def SvcDescriptionItems(self):
		return {
			**super(StatusWriter, self).SvcDescriptionItems(),
			**{
				'FilePath'      : self.filePath,
			}
		}
