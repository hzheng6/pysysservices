#!/usr/bin/env python3
# -*- coding:utf-8 -*-
###
# Copyright (c) 2022 Haofan Zheng
# Use of this source code is governed by an MIT-style
# license that can be found in the LICENSE file or at
# https://opensource.org/licenses/MIT.
###


import os
import logging

from ..DHomeHttpsServer.AuthUtils import ApiKeyAuth
from ..DHomeHttpsServer.Client import (
	ReqBodyJson,
	Request,
)
from ..DHomeHttpsServer.Client.SSLContext import GetRegularClientContext

from ..DHomeKeyManager.DHomeKeyManager.keyring import KEY_TYPE_MANAGER_MAP
from ..DHomeKeyManager.DHomeKeyManager.utils import ConvertCertSubjectInfo

from .. import ServiceFunction
from .. import Utils


class DHomeCertNew(ServiceFunction.ServiceFunction):

	def __init__(self, svcStateDir, config, globalConf):

		isBatch = globalConf['isBatch']
		if isBatch:
			raise RuntimeError(
				'This function is not available under batch mode'
			)

		uid        = config['uniId']
		keyType    = config['keyType']
		hostname   = config['hostname']

		svcId = Utils.CalcSvcId(
			'{}.{}.{}.{}.{}'.format(
				__name__,
				self.__class__.__name__,
				keyType,
				hostname,
				uid,
			)
		)

		super(DHomeCertNew, self).__init__(
			svcId, config, globalConf, svcStateDir)

		self.logger = logging.getLogger(
			__name__ + '.' + self.__class__.__name__)

		self.uid = uid
		self.kmsSvrAddr = config['kmsSvrAddr']
		self.kmsSvrPort = config['kmsSvrPort']
		self.kmsSvrCA   = config['kmsSvrCA']
		self.outPrivKey = config['outPrivKey']
		self.outCert    = config['outCert']
		self.keyType    = config['keyType']
		self.hostname   = config['hostname']
		self.domains    = config['domains']
		self.validDays  = config['validDays']

		self.briefOutCert = Utils.BriefText(
			self.outCert, 43)

		self.svcType = "OneShot"


	def RunService(self, globalState, globalCache):
		keyMgrCls = KEY_TYPE_MANAGER_MAP[self.keyType]
		keyMgr = keyMgrCls()
		keyMgr.GenKey(outPrivKeyPath=self.outPrivKey)

		csrPem = keyMgr.GenCsrPem(
			ConvertCertSubjectInfo({ 'CN': self.hostname })
		)

		cltTlsCtx = GetRegularClientContext(
			caCertPath=self.kmsSvrCA,
		)

		self.username = input('Please enter username: ')
		self.dynPin   = input('Please enter PIN: ')

		reqBody = {
			'keyType' : self.keyType,
			'hostname': self.hostname,
			'domains' : self.domains,
			'validFor': self.validDays,
			'csrPem'  : csrPem,
			'authBy'  : self.username,
		}

		authCode = ApiKeyAuth.GenAuthCode(
			apiKey=self.dynPin,
			authContent=[
				reqBody['keyType'].encode(),
				reqBody['hostname'].encode(),
				*[ x.encode() for x in reqBody['domains'] ],
				str(reqBody['validFor']).encode(),
				reqBody['csrPem'].encode(),
				reqBody['authBy'].encode(),
			],
			digest='sha3_512'
		)

		reqBody = ReqBodyJson.ReqBodyJson({
			**reqBody,
			'authCode': authCode,
		})

		respRaw, respType, respParsed = Request.PostRequest(
			sslContext=cltTlsCtx,
			host=self.kmsSvrAddr,
			port=self.kmsSvrPort,
			uri='/new_cert/',
			reqBody=reqBody,
		)
		if respType != 'json':
			raise TypeError('Unexpected response type {}'.format(respType))

		with open(self.outCert + '.tmp', 'wb') as f:
			f.write(respParsed['certPem'].encode())
			for cert in respParsed['caPems']:
				f.write(cert.encode())
		os.replace(self.outCert + '.tmp', self.outCert)


	def GetStatus(self):
		return {
			**super(DHomeCertNew, self).GetStatus(),
			**{
				'Hostname'      : self.hostname,
				'KeyType'       : self.keyType,
				'OutputCert'    : self.briefOutCert,
			}
		}


	def SvcDescriptionItems(self):
		return {
			**super(DHomeCertNew, self).SvcDescriptionItems(),
			**{
				'Hostname'      : self.hostname,
				'KeyType'       : self.keyType,
				'OutputCert'    : self.briefOutCert,
			}
		}
