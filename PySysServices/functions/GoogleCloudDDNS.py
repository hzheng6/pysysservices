#!/usr/bin/env python3
# -*- coding:utf-8 -*-
###
# Copyright (c) 2022 Haofan Zheng
# Use of this source code is governed by an MIT-style
# license that can be found in the LICENSE file or at
# https://opensource.org/licenses/MIT.
###


import logging
import time

from typing import List, Union

from googleapiclient import discovery
# python3 -m pip install google-api-python-client
from google.oauth2 import service_account
# python3 -m pip install google-auth

from .. import DdnsSelector
from .. import RepeatTaskSafeGuard
from .. import ServiceFunction
from .. import Utils


class GoogleCloudDDNSMgr(object):
	def __init__(self,
		svcAccInfo: dict,
		projectId: str,
		zoneId: str,
		domainName: str,
		recordName: str,
		ttl: int):
		super(GoogleCloudDDNSMgr, self).__init__()

		scopes = [
			'https://www.googleapis.com/auth/ndev.clouddns.readonly',
			'https://www.googleapis.com/auth/ndev.clouddns.readwrite',
		]
		self.credential = service_account.Credentials.from_service_account_info(
			svcAccInfo, scopes=scopes )
		self.projectId = projectId
		self.zoneId = zoneId
		self.domainName = (domainName
			if domainName.endswith('.') else
			(domainName + '.'))
		self.recordName = (recordName[:-1]
			if recordName.endswith('.') else
			recordName)
		self.name = '{}.{}'.format(self.recordName, self.domainName)
		self.ttl = ttl

		self.logger = logging.getLogger(
			__name__ + '.' + self.__class__.__name__ + '.' + self.name)

		self.service = discovery.build(
			'dns', 'v1', credentials=self.credential)


	def GetCurrentRec(self, type: str) -> Union[None, List[str]]:
		request = self.service.resourceRecordSets().list(
			project=self.projectId,
			managedZone=self.zoneId,
			name=self.name,
			type=type)
		response = request.execute()

		self.logger.info('Received record list: {}'.format(response))
		if ('rrsets' in response) and (len(response['rrsets']) > 0):
			return response['rrsets'][0]['rrdatas']
		else:
			return None


	def CreateRec(self, type: str, data: List[str]) -> None:
		dnsRecBody = {
			'kind': 'dns#resourceRecordSet',
			'name': self.name,
			'rrdatas': data,
			'ttl': self.ttl,
			'type': type,
		}
		request = self.service.resourceRecordSets().create(
			project=self.projectId,
			managedZone=self.zoneId,
			body=dnsRecBody)
		response = request.execute()

		self.logger.info('Created record: {}'.format(response))


	def UpdateRec(self, type: str, data: List[str]) -> None:
		dnsRecBody = {
			'kind': 'dns#resourceRecordSet',
			'name': self.name,
			'rrdatas': data,
			'ttl': self.ttl,
			'type': type,
		}
		request = self.service.resourceRecordSets().patch(
			project=self.projectId,
			managedZone=self.zoneId,
			name=self.name,
			type=type,
			body=dnsRecBody)
		response = request.execute()

		self.logger.info('Updated record: {}'.format(response))


class GoogleCloudDDNS(ServiceFunction.ServiceFunction):

	def __init__(self, svcStateDir, config, globalConf):
		domainName = config['domain']
		recName = config['recName']

		svcId = Utils.CalcSvcId(
			'{}.{}.{}.{}'.format(
				__name__, self.__class__.__name__, recName, domainName))

		super(GoogleCloudDDNS, self).__init__(
			svcId, config, globalConf, svcStateDir)

		self.logger = logging.getLogger(
			'{}.{}.{}.{}'.format(
				__name__, self.__class__.__name__, recName, domainName))

		self.dataSrc = config['dataSrc']
		self.selectors = DdnsSelector.ConstructSelectors(config['selector'])
		self.domainName = domainName
		self.recName = recName
		self.ttl = config['ttl']
		self.projectId = config['projectId']
		self.zoneId = config['zoneId']
		self.credential = config['credential']

		self.repSafeGuard = RepeatTaskSafeGuard.RepeatTaskSafeGuard(60)

		self.ddnsMgr = GoogleCloudDDNSMgr(
			self.credential,
			self.projectId,
			self.zoneId,
			self.domainName,
			self.recName,
			self.ttl)

		self.lastRec = [
			self.ddnsMgr.GetCurrentRec(selector.GetType())
			for selector in self.selectors
		]

		self.svcType = "Daemon"


	def _SafeGuardTarget(
		self,
		idx: int,
		type: str,
		newList: List[str]
	) -> None:

		self.logger.info(
			'Updating {} type record to {}...'.format(
				type, newList))

		try:
			if self.lastRec[idx] is None:
				# there is no existing record; take create route
				self.ddnsMgr.CreateRec(type, newList)
			else:
				# there is existing record; take update route
				self.ddnsMgr.UpdateRec(type, newList)

			self.lastRec[idx] = newList
		except Exception as e:
			self.logger.error(
				'Failed to update record with exception: {}'.format(e)
			)


	def RunService(self, globalState: dict, globalCache: dict):
		while not self.isTerminating:

			for idx in range(len(self.selectors)):
				selector = self.selectors[idx]
				lastRec = self.lastRec[idx]

				newData = []
				for srcPath in self.dataSrc:
					newData += selector.Select(
						Utils.FetchSubDataFromDataset(globalCache, srcPath))

				if ((lastRec is None) or
					(not selector.IsEqual(lastRec, newData))):
					# New data is different from last data
					# We need to update the record

					self.repSafeGuard.Run(
						target=self._SafeGuardTarget,
						args=(idx, selector.GetType(), newData)
					)

			time.sleep(self.updInterval)


	def GetStatus(self):
		return {
			**super(GoogleCloudDDNS, self).GetStatus(),
			**{
				'Domain'     : self.domainName,
				'Record'     : self.recName,
				'Selectors'  : [ x.__class__.__name__ for x in self.selectors ],
				'LastRecord' : self.lastRec,
			}
		}


	def SvcDescriptionItems(self):
		return {
			**super(GoogleCloudDDNS, self).SvcDescriptionItems(),
			**{
				'Domain'     : self.domainName,
				'Record'     : self.recName,
			}
		}
