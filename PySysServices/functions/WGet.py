#!/usr/bin/env python3
# -*- coding:utf-8 -*-
###
# Copyright (c) 2022 Haofan Zheng
# Use of this source code is governed by an MIT-style
# license that can be found in the LICENSE file or at
# https://opensource.org/licenses/MIT.
###


import os
import logging
import time

from .. import HttpsSource
from .. import RepeatTaskSafeGuard
from .. import ServiceFunction
from .. import Utils


class WGet(ServiceFunction.ServiceFunction):

	def __init__(self, svcStateDir, config, globalConf):

		destPath = os.path.abspath(config['destPath'])
		svcId = Utils.CalcSvcId(
			'{}.{}.{}'.format(__name__, self.__class__.__name__, destPath))

		super(WGet, self).__init__(
			svcId, config, globalConf, svcStateDir)

		self.logger = logging.getLogger(
			__name__ + '.' + self.__class__.__name__)

		self.srcRoot = config['sourceRoot']
		self.srcPath = config['sourcePath'].strip()
		self.destPath = destPath

		self.reGet = Utils.ProcOptionalConfig(
			config, 'reGet', "Never")
		self.reGetDelay = Utils.ProcOptionalConfig(
			config, 'reGetDelay', 10)

		self.briefDestPath = Utils.BriefText(self.destPath, 43)

		if self.reGet == "Never":
			self.svcType = "OneShot"
		elif self.reGet == "Always":
			self.svcType = "Daemon"
		else:
			raise ValueError('Invalid reGet value - {}'.format(self.reGet))

		self.repSafeGuard = RepeatTaskSafeGuard.RepeatTaskSafeGuard(
			self.reGetDelay)


	def _SafeGuardRunTarget(self):
		self.logger.debug('Downloading {}/{} to {}'.format(
			self.srcRootMgr.addr, self.srcPath, self.briefDestPath))

		Utils.AtomicWriteFile(
			self.destPath,
			self.srcRootMgr.FetchContent(self.srcPath),
			'wb')


	def RunService(self, globalState, globalCache):
		self.srcRootMgr: HttpsSource.HttpsSource = (
			globalCache['sources'][self.srcRoot])

		destDir = os.path.dirname(self.destPath)
		os.makedirs(destDir, exist_ok=True)

		if self.reGet == "Always":
			while not self.isTerminating:
				self.repSafeGuard.Run(self._SafeGuardRunTarget)
				time.sleep(self.updInterval)
		elif self.reGet == "Never":
			self.repSafeGuard.Run(self._SafeGuardRunTarget)
		else:
			raise ValueError('Invalid reGet value - {}'.format(self.reGet))


	def GetStatus(self):
		return {
			**super(WGet, self).GetStatus(),
			**{
				'SourcePath'        : self.srcPath,
				'DestinationPath'   : self.destPath,
				'ReGet'             : self.reGet,
			}
		}


	def SvcDescriptionItems(self):
		return {
			**super(WGet, self).SvcDescriptionItems(),
			**{
				'Destination'     : self.briefDestPath,
				'ReGet'           : self.reGet,
			}
		}
