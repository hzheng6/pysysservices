#!/usr/bin/env python3
# -*- coding:utf-8 -*-
###
# Copyright (c) 2022 Haofan Zheng
# Use of this source code is governed by an MIT-style
# license that can be found in the LICENSE file or at
# https://opensource.org/licenses/MIT.
###


import logging
import signal
import time

from .. import ServiceFunction
from .. import Utils


class WaitForInterrupt(ServiceFunction.ServiceFunction):

	def __init__(self, svcStateDir, config, globalConf):

		svcId = Utils.CalcSvcId(
			'{}.{}'.format(__name__, self.__class__.__name__))

		super(WaitForInterrupt, self).__init__(
			svcId, config, globalConf, svcStateDir)

		self.logger = logging.getLogger(
			__name__ + '.' + self.__class__.__name__)

		self.svcType = 'OneShot'
		self.isWaiting = False
		self.received = None


	def Handler(self, sigNum: int, frame: object):
		sigRecvName = signal.Signals(sigNum).name
		self.logger.info('Received signal: {}'.format(sigRecvName))
		self.received = sigNum


	def RunService(self, globalState, globalCache):
		sigSet = [signal.SIGINT, signal.SIGTERM]
		if getattr(signal, 'SIGBREAK', None):
			sigSet.append(signal.SIGBREAK)

		# Register signal handler
		for sigNum in sigSet:
				signal.signal(sigNum, self.Handler)

		# begin to wait for signals
		self.logger.info('Waiting for interrupt/termination signal')
		self.isWaiting = True
		while self.received is None:
			time.sleep(self.updInterval)

		# signal received
		self.isWaiting = False


	def StopService(self):
		pass


	def GetStatus(self):
		return {
			**super(WaitForInterrupt, self).GetStatus(),
			**{
			'IsWaiting'      : self.isWaiting,
			}
		}
