#!/usr/bin/env python3
# -*- coding:utf-8 -*-
###
# Copyright (c) 2022 Haofan Zheng
# Use of this source code is governed by an MIT-style
# license that can be found in the LICENSE file or at
# https://opensource.org/licenses/MIT.
###


import os
import logging
import subprocess
import time
from typing import List

from cryptography import x509
from cryptography.hazmat.primitives.serialization import (
	pkcs12,
	BestAvailableEncryption,
	load_pem_private_key,
)

from .. import RepeatTaskSafeGuard
from .. import ServiceFunction
from .. import Utils


class UnifiUpdKeyCert(ServiceFunction.ServiceFunction):

	def __init__(self, svcStateDir, config, globalConf):

		uid = config['uniId']

		svcId = Utils.CalcSvcId(
			'{}.{}.{}'.format(
				__name__,
				self.__class__.__name__,
				uid,
			)
		)

		super(UnifiUpdKeyCert, self).__init__(
			svcId, config, globalConf, svcStateDir)

		self.logger = logging.getLogger(
			__name__ + '.' + self.__class__.__name__)

		self.uid = uid
		self.p12Output = config['p12Output']
		self.inPrivKey = config['inPrivKey']
		self.inCert = config['inCert']
		self.outKeystore = config['outKeystore']
		self.ksUpdInterval = config['ksUpdInterval']

		self.unifiName = 'unifi'
		self.unifiPass = 'aircontrolenterprise'

		if not os.path.isfile(self.inPrivKey):
			raise RuntimeError('Private key file not found')

		if not os.path.isfile(self.inCert):
			raise RuntimeError('Certificate file not found')

		self.briefInPrivKey  = Utils.BriefText(
			self.inPrivKey, 43)
		self.briefInCert = Utils.BriefText(
			self.inCert, 43)
		self.briefOutKeystore = Utils.BriefText(
			self.outKeystore, 43)

		self.repSafeGuard = RepeatTaskSafeGuard.RepeatTaskSafeGuard(
			self.ksUpdInterval
		)

		self.svcType = "Daemon"


	def _ReadPemChain(self) -> List[str]:
		chain = []
		with open(self.inCert, 'r') as f:
			s = f.read()
			startPos = 0
			endPos = s.find('-----END CERTIFICATE-----')
			while endPos != -1:
				chain.append(s[startPos:endPos + 25])
				startPos = endPos + 26
				endPos = s.find('-----END CERTIFICATE-----', startPos)

		if len(chain) == 0:
			raise RuntimeError('No certificate found in the chain')

		return chain


	def _SafeGuardTarget(self) -> None:
		# 1. certificate & key ==> pkcs12
		privKey = load_pem_private_key(
			data=open(self.inPrivKey, 'rb').read(),
			password=None,
		)

		cas = self._ReadPemChain()
		cas = [ x.encode() for x in cas ]

		cert, cas = cas[0], cas[1:]

		if len(cas) == 0:
			raise RuntimeError('No CA certificate found in the chain')

		cert = x509.load_pem_x509_certificate(cert)
		cas = [ x509.load_pem_x509_certificate(x) for x in cas ]

		p12Bytes = pkcs12.serialize_key_and_certificates(
			name=self.unifiName.encode(),
			key=privKey,
			cert=cert,
			cas=cas,
			encryption_algorithm=BestAvailableEncryption(
				password=self.unifiPass.encode()
			),
		)

		with open(self.p12Output + '.tmp', 'wb') as f:
			f.write(p12Bytes)
			f.flush()
		os.replace(self.p12Output + '.tmp', self.p12Output)

		# 2. pkcs12 ==> keystore
		cmd = [
			'keytool',
			'-importkeystore',
			'-deststorepass', self.unifiPass,
			'-destkeypass',   self.unifiPass,
			'-destkeystore',  self.outKeystore,
			'-srckeystore',   self.p12Output,
			'-srcstoretype',  'pkcs12',
			'-srcstorepass',  self.unifiPass,
			'-alias',         self.unifiName,
			'-noprompt',
			'-v'
		]
		progOutput = os.path.join(
				self.svcStateDir, 'pout-' + self.svcId + '.log')
		with open(progOutput, 'a') as logFile:
			self.proc = subprocess.Popen(
				cmd,
				stdout=logFile,
				stderr=logFile,
			)

		# 3. restart unifi
		cmd = [
			'systemctl',
			'restart',
			'unifi',
		]
		with open(progOutput, 'a') as logFile:
			self.proc = subprocess.Popen(
				cmd,
				stdout=logFile,
				stderr=logFile,
			)

		self.logger.info('Finished updating the Unifi keystore')


	def RunService(self, globalState, globalCache) -> None:
		while not self.isTerminating:

			try:
				self.repSafeGuard.Run(target=self._SafeGuardTarget)
			except Exception as e:
				self.logger.error(
					'Failed to update unifi keystore ' +
					'with error: {}'.format(e)
				)

			time.sleep(self.updInterval)


	def GetStatus(self):
		return {
			**super(UnifiUpdKeyCert, self).GetStatus(),
			**{
				'InPrivKey'      : self.inPrivKey,
				'InCert'         : self.inCert,
				'OutKeystore'    : self.outKeystore,
			}
		}


	def SvcDescriptionItems(self):
		return {
			**super(UnifiUpdKeyCert, self).SvcDescriptionItems(),
			**{
				'InPrivKey'      : self.briefInPrivKey,
				'InCert'         : self.briefInCert,
				'OutKeystore'    : self.briefOutKeystore,
			}
		}
