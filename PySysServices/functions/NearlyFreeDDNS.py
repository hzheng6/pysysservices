#!/usr/bin/env python3
# -*- coding:utf-8 -*-
###
# Copyright (c) 2022 Haofan Zheng
# Use of this source code is governed by an MIT-style
# license that can be found in the LICENSE file or at
# https://opensource.org/licenses/MIT.
###


import ssl
import http.client
import hashlib
import logging
import random
import urllib
import time
import json

from typing import List, Union

from .. import DdnsSelector
from .. import RepeatTaskSafeGuard
from .. import ServiceFunction
from .. import Utils


API_HOST_ADDR = 'api.nearlyfreespeech.net'

_SALT_ALPHABET = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ'
_SALT_LENGTH = 16


def _GenSalt():
	return ''.join([random.choice(_SALT_ALPHABET) for i in range(_SALT_LENGTH)])


def _GenTimeStamp():
	return str(int(time.time()))


def _GetConnection():

	sslCtx = ssl.create_default_context(ssl.Purpose.SERVER_AUTH)
	sslCtx.verify_mode = ssl.CERT_REQUIRED
	sslCtx.check_hostname = True
	sslCtx.minimum_version = ssl.TLSVersion.TLSv1_2

	conn = http.client.HTTPSConnection(
		API_HOST_ADDR,
		context=sslCtx,
		timeout=60
	)

	return conn


def _CalcBodyHash(encBody: dict):
	return hashlib.sha1(encBody).hexdigest()


class NearlyFreeDDNSMgr(object):

	def __init__(self, domain, recName, ttl, apiUsername, apiKey):
		super(NearlyFreeDDNSMgr, self).__init__()

		self.domain = domain
		self.recName = recName
		self.ttl = ttl
		self.apiUsername = apiUsername
		self.apiKey = apiKey

		self.logger = logging.getLogger(
			__name__ + '.' + self.__class__.__name__ +
			'.' + self.domain + '.' + self.recName
		)


	def _GetUri(self, func: str):
		return '/dns/' + self.domain + '/' + func


	def _GenAuthHeader(self,
		uri: str,
		bodyHash: str
	):
		salt = _GenSalt()
		timestamp = _GenTimeStamp()

		#Calc hash: username;timestamp;salt;apiKey;uri;BodyHash
		preHashStr = ';'.join([self.apiUsername, timestamp, salt, self.apiKey, uri, bodyHash])
		authHash = hashlib.sha1(preHashStr.encode('utf-8')).hexdigest()

		authHeader = ';'.join([self.apiUsername, timestamp, salt, authHash])

		return {'X-NFSN-Authentication': authHeader}


	def _SendRequest(self,
		uri: str,
		body: dict,
	) -> Union[bytes, None]:

		encodedBody = urllib.parse.urlencode(body).encode('utf-8')
		bodyHash = _CalcBodyHash(encodedBody)

		authHeader = self._GenAuthHeader(uri, bodyHash)
		headers = {
			'Content-Type': 'application/x-www-form-urlencoded',
			'Content-Length': len(encodedBody),
			**authHeader
		}

		self.logger.info('Sending request to {}...'.format(uri))
		conn = _GetConnection()

		conn.request('POST', uri, body=encodedBody, headers=headers)

		resp = conn.getresponse()

		respLenStr = resp.getheader('Content-Length')

		if resp.status != 200:
			raise RuntimeError('Server Resp {}'.format(resp.status))

		if respLenStr is None:
			return None
		else:
			respLen = int(respLenStr)

			return resp.read(respLen)


	def GetCurrentRec(self, type: str) -> List[str]:

		uri = self._GetUri('listRRs')
		body = {
			'name': self.recName,
			'type': type
		}

		resJson = json.loads(self._SendRequest(uri, body))

		return [
			rec['data'] for rec in resJson
			if rec['name'] == self.recName ]


	def RemoveCurrentRec(self, type: str, currData: str) -> None:

		uri = self._GetUri('removeRR')
		body = {
			'name': self.recName,
			'type': type,
			'data': currData
		}

		self._SendRequest(uri, body)
		self.logger.info('Removed old record with data {}'.format(currData))


	def AddCurrentRec(self, type: str, data: str) -> None:

		uri = self._GetUri('addRR')
		body = {
			'name': self.recName,
			'type': type,
			'data': data,
			'ttl': self.ttl
		}

		self._SendRequest(uri, body)
		self.logger.info('Added new record with data {}'.format(data))


class NearlyFreeDDNS(ServiceFunction.ServiceFunction):

	def __init__(self, svcStateDir, config, globalConf):

		domainName = config['domain']
		recName = config['recName']

		svcId = Utils.CalcSvcId(
			'{}.{}.{}.{}'.format(
				__name__, self.__class__.__name__, recName, domainName))

		super(NearlyFreeDDNS, self).__init__(
			svcId, config, globalConf, svcStateDir)

		self.logger = logging.getLogger(
			'{}.{}.{}.{}'.format(
				__name__, self.__class__.__name__, recName, domainName))

		self.dataSrc = config['dataSrc']
		self.selectors = DdnsSelector.ConstructSelectors(config['selector'])
		self.domainName = domainName
		self.recName = recName
		self.ttl = config['ttl']
		self.apiUsername = config['apiUsername']
		self.apiKey = config['apiKey']

		self.repSafeGuard = RepeatTaskSafeGuard.RepeatTaskSafeGuard(60)

		self.ddnsMgr = NearlyFreeDDNSMgr(
			self.domainName, self.recName, self.ttl, self.apiUsername, self.apiKey)

		self._GenLastRec()
		self.hadReqError = False

		self.svcType = "Daemon"


	def _GenLastRec(self):
		self.lastRec = [
			self.ddnsMgr.GetCurrentRec(selector.GetType())
			for selector in self.selectors
		]


	def _SafeGuardTargetEdit(
		self,
		idx: int,
		type: str,
		removeList: List[str],
		addList: List[str]
	) -> None:

		self.logger.info(
			'Updating {} type record; removing {} adding {}...'.format(
				type, removeList, addList))

		try:
			for rec in removeList:
				self.ddnsMgr.RemoveCurrentRec(type, rec)
				self.lastRec[idx].remove(rec)

			for rec in addList:
				self.ddnsMgr.AddCurrentRec(type, rec)
				self.lastRec[idx].append(rec)

			self.hadReqError = False
		except Exception as e:
			self.hadReqError = True
			self.logger.error(
				'Failed to update record with exception: {}'.format(e)
			)


	def _SafeGuardTargetGet(
		self
	) -> None:

		self.logger.info('Getting last record again...')

		try:
			self._GenLastRec()
			self.hadReqError = False
		except Exception as e:
			self.hadReqError = True
			self.logger.error(
				'Failed to get last record with exception: {}'.format(e)
			)


	def RunService(self, globalState: dict, globalCache: dict):
		while not self.isTerminating:

			if self.hadReqError:
				self.repSafeGuard.Run(
					target=self._SafeGuardTargetGet,
				)
			else:
				for idx in range(len(self.selectors)):
					selector = self.selectors[idx]
					lastRec = self.lastRec[idx]

					newData = []
					for srcPath in self.dataSrc:
						newData += selector.Select(
							Utils.FetchSubDataFromDataset(globalCache, srcPath)
						)

					if not selector.IsEqual(lastRec, newData):
						# New data is different from last data
						# We need to update the record

						removeList, addList = selector.GenStrategy(
							lastRec,
							newData
						)
						self.repSafeGuard.Run(
							target=self._SafeGuardTargetEdit,
							args=(idx, selector.GetType(), removeList, addList)
						)

			time.sleep(self.updInterval)


	def GetStatus(self):
		return {
			**super(NearlyFreeDDNS, self).GetStatus(),
			**{
				'Domain'     : self.domainName,
				'Record'     : self.recName,
				'Selectors'  : [ x.__class__.__name__ for x in self.selectors ],
				'LastRecord' : self.lastRec,
				'hadReqErr'  : self.hadReqError,
			}
		}


	def SvcDescriptionItems(self):
		return {
			**super(NearlyFreeDDNS, self).SvcDescriptionItems(),
			**{
				'Domain'     : self.domainName,
				'Record'     : self.recName,
			}
		}
