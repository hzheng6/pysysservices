#!/usr/bin/env python3
# -*- coding:utf-8 -*-
###
# Copyright (c) 2022 Haofan Zheng
# Use of this source code is governed by an MIT-style
# license that can be found in the LICENSE file or at
# https://opensource.org/licenses/MIT.
###


import logging
import subprocess
import sys
import time

from .. import RepeatTaskSafeGuard
from .. import ServiceFunction
from .. import Utils


class NetSvcResetter(ServiceFunction.ServiceFunction):

	def __init__(self, svcStateDir, config, globalConf):

		nicName = config['nic']
		sysSvcName = config['svc']

		svcId = Utils.CalcSvcId(
			'{}.{}.{}.{}'.format(
				__name__, self.__class__.__name__, nicName, sysSvcName))

		super(NetSvcResetter, self).__init__(
			svcId, config, globalConf, svcStateDir)

		self.logger = logging.getLogger(
			__name__ + '.' + self.__class__.__name__ + '.' + nicName)

		self.nicName = nicName
		self.sysSvcName = sysSvcName

		self.isNicOk = True
		self.repSafeGuard = RepeatTaskSafeGuard.RepeatTaskSafeGuard(30)

		self.selfHealWait = 30
		self.noOkSince = 0

		self.svcType = "Daemon"


	def _SafeGuardTarget(self):
		self.logger.info('Resetting service {}'.format(self.sysSvcName))
		cmd = ['systemctl', 'restart', self.sysSvcName]
		proc = subprocess.Popen(cmd,
				stdin=sys.stdin, stdout=sys.stdout, stderr=sys.stderr)
		proc.wait()
		self.logger.info('process exited with code {}'.format(proc.returncode))


	def RunService(self, globalState, globalCache):
		while not self.isTerminating:
			if (('NICStatus' in globalCache) and
				(self.nicName in globalCache['NICStatus'])):
				ipv4AddrList = globalCache['NICStatus'][self.nicName]['ipv4']
				if ((len(ipv4AddrList) > 0) and
					('addr' in ipv4AddrList[0]) and
					(ipv4AddrList[0]['addr'] != '')):
					self.isNicOk = True
				else:
					# NIC is not OK
					if self.isNicOk:
						# We detected NIC down since this point
						self.logger.info('NIC {} is down'.format(self.nicName))
						self.noOkSince = time.time()

						self.isNicOk = False


			if ((not self.isNicOk) and # NIC is down
				((time.time() - self.noOkSince) > self.selfHealWait)
				# and self-heal time has passed
			):
				self.repSafeGuard.Run(target=self._SafeGuardTarget)


			time.sleep(self.updInterval)


	def GetStatus(self):
		return {
			**super(NetSvcResetter, self).GetStatus(),
			**{
				'NICName'      : self.nicName,
				'ServiceName'  : self.sysSvcName,
				'IsNICOk'      : self.isNicOk,
				'NICDownSince' : self.noOkSince,
			}
		}


	def SvcDescriptionItems(self):
		return {
			**super(NetSvcResetter, self).SvcDescriptionItems(),
			**{
				'NICName'      : self.nicName,
				'ServiceName'  : self.sysSvcName,
			}
		}
