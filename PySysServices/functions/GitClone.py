#!/usr/bin/env python3
# -*- coding:utf-8 -*-
###
# Copyright (c) 2022 Haofan Zheng
# Use of this source code is governed by an MIT-style
# license that can be found in the LICENSE file or at
# https://opensource.org/licenses/MIT.
###


import os
import logging

import git # python3 -m pip install GitPython

from .. import ServiceFunction
from .. import Utils

class GitClone(ServiceFunction.ServiceFunction):

	def __init__(self, svcStateDir, config, globalConf):

		gitAddr = config['address']
		destPath = os.path.abspath(config['destPath'])

		svcId = Utils.CalcSvcId(
			'{}.{}.{}.{}'.format(
				__name__, self.__class__.__name__, gitAddr, destPath))

		super(GitClone, self).__init__(svcId, config, globalConf, svcStateDir)

		self.logger = logging.getLogger(
			__name__ + '.' + self.__class__.__name__)

		self.gitAddr = gitAddr
		self.destPath = destPath
		self.branch = config['branch']

		self.briefDestPath = Utils.BriefText(self.destPath, 43)

		self.svcType = "OneShot"


	def RunService(self, globalState, globalCache):
		dirPath = os.path.dirname(self.destPath)
		os.makedirs(dirPath, exist_ok=True)

		repo = git.Repo.clone_from(self.gitAddr, self.destPath,
			multi_options=['--recurse-submodules', '--single-branch'],
			branch=self.branch
		)

		verHash = repo.head.commit.hexsha

		self.logger.info(
			'Cloned git repo {} into {}, with version {} authored by {}.'.format(
			self.gitAddr, self.briefDestPath, verHash[:8],
			repo.head.commit.author.name
		))


	def GetStatus(self):
		return {
			**super(GitClone, self).GetStatus(),
			**{
				'GitAddress'      : self.gitAddr,
				'Destination'     : self.destPath,
				'Branch'          : self.branch,
			}
		}


	def SvcDescriptionItems(self):
		return {
			**super(GitClone, self).SvcDescriptionItems(),
			**{
				'Destination'     : self.briefDestPath,
			}
		}
