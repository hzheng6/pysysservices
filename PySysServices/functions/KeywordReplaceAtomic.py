#!/usr/bin/env python3
# -*- coding:utf-8 -*-
###
# Copyright (c) 2022 Haofan Zheng
# Use of this source code is governed by an MIT-style
# license that can be found in the LICENSE file or at
# https://opensource.org/licenses/MIT.
###


import os
import logging

from .. import ServiceFunction
from .. import Utils

class KeywordReplaceAtomic(ServiceFunction.ServiceFunction):

	def __init__(self, svcStateDir, config, globalConf):

		path = os.path.abspath(config['path'])
		repPairs = [(x[0], x[1]) for x in config['replacePairs']]
		uid = config['uniId']

		svcId = Utils.CalcSvcId(
			'{}.{}.{}.{}.{}'.format(
				__name__, self.__class__.__name__, path, repPairs, uid))

		super(KeywordReplaceAtomic, self).__init__(
			svcId, config, globalConf, svcStateDir)

		self.logger = logging.getLogger(
			__name__ + '.' + self.__class__.__name__)

		self.path = path
		self.repPairs = repPairs
		self.uid = uid

		self.briefDestPath = Utils.BriefText(self.path, 43)

		self.svcType = "OneShot"


	def RunService(self, globalState, globalCache):
		with open(self.path, 'r') as inFile:
			content = inFile.read()

			for item in self.repPairs:
				content = content.replace(item[0], item[1])

			Utils.AtomicWriteFile(self.path, content, 'w')


	def GetStatus(self):
		return {
			**super(KeywordReplaceAtomic, self).GetStatus(),
			**{
				'SourcePath'        : self.path,
				'ReplacingPair'     : self.repPairs,
			}
		}


	def SvcDescriptionItems(self):
		return {
			**super(KeywordReplaceAtomic, self).SvcDescriptionItems(),
			**{
				'Destination'     : self.briefDestPath,
			}
		}
