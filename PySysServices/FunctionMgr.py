#!/usr/bin/env python3
# -*- coding:utf-8 -*-
###
# Copyright (c) 2022 Haofan Zheng
# Use of this source code is governed by an MIT-style
# license that can be found in the LICENSE file or at
# https://opensource.org/licenses/MIT.
###


import importlib.util
import logging
import os

from typing import Dict
from . import ServiceFunction


FUNCTION_CLS_MAP: Dict[str, ServiceFunction.ServiceFunction] = {}


def GetFunction(name: str) -> ServiceFunction.ServiceFunction:
	if name not in FUNCTION_CLS_MAP:
		logger = logging.getLogger(__name__ + '.' + GetFunction.__name__)

		# get the path to the module
		filePath = os.path.realpath(__file__)
		filePath = os.path.split(filePath)[0]
		filePath = os.path.join(filePath, 'functions', name + '.py')

		# construct the module name within the package
		pkgName = __name__
		pkgName = pkgName[:pkgName.rfind('.')] # take out the "FunctionMgr" part
		pkgName = pkgName + '.functions.' + name

		# load the module
		logger.info('Loading function {} from {}'.format(pkgName, filePath))
		spec = importlib.util.spec_from_file_location(pkgName, filePath)
		loadedMod = importlib.util.module_from_spec(spec)
		spec.loader.exec_module(loadedMod)

		FUNCTION_CLS_MAP[name] = (spec, loadedMod, getattr(loadedMod, name))

	return FUNCTION_CLS_MAP[name][2]
