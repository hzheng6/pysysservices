#!/usr/bin/env python3
# -*- coding:utf-8 -*-
###
# Copyright (c) 2022 Haofan Zheng
# Use of this source code is governed by an MIT-style
# license that can be found in the LICENSE file or at
# https://opensource.org/licenses/MIT.
###


import argparse
import logging
import json
import sys
import os
from typing import List

from PySysServices import FunctionMgr
from PySysServices import HttpsSource
from PySysServices import ServiceFunction
from PySysServices import Utils


def ReadConfig(path: str) -> dict:
	with open(path, 'r') as file:
		return json.loads(file.read())


def LoadGlobalState(globalConf: dict) -> dict:
	statePath = os.path.join(globalConf['stateDir'], 'globalStates.json')
	if os.path.isfile(statePath):
		return ReadConfig(statePath)
	else:
		return {}


def LoadSources(config, globalConf):

	logger = logging.getLogger(__name__ + '.' + LoadSources.__name__)
	srcs = {}
	for key, val in config.items():
		if val['type'] == 'https':
			srcs[key] = HttpsSource.HttpsSource(key, globalConf)
			logger.info('Loaded source {}'.format(key))
		else:
			raise TypeError('Source type of {} is not supported.'.format(val['type']))

	return srcs


def InitServices(
	serviceList: List[dict],
	globalConf: dict,
	svcStateDir: str) -> List[ServiceFunction.ServiceFunction]:

	services = []
	logger = logging.getLogger(__name__ + '.' + InitServices.__name__)

	for svc in serviceList:
		funcName = svc['function']
		try:
			func = FunctionMgr.GetFunction(funcName)
			try:
				services.append(func(svcStateDir, svc, globalConf))
			except Exception as e:
				logger.error(
					'Failed to initialize service {} with error - {}'.format(
						funcName, e))
		except Exception as e:
			logger.error(
				'Failed to load function {} with error - {}'.format(
					funcName, e))

	return services


def SetupLogger(logLevel, logFile = None, logStream = None):

	logging.root.setLevel(logLevel)
	logFormatter = logging.Formatter(
		fmt='%(asctime)s [%(levelname)s]%(name)s - %(message)s',
		datefmt='%Y-%m-%dT%H:%M:%S%z',
	)

	if logStream is not None:
		logHdlr = logging.StreamHandler(logStream)
		logHdlr.setFormatter(logFormatter)
		logging.root.addHandler(logHdlr)

	if logFile is not None:
		logHdlr = logging.FileHandler(logFile)
		logHdlr.setFormatter(logFormatter)
		logging.root.addHandler(logHdlr)

	if logFile is not None:
		logging.root.info('Log will be written to file {}'.format(logFile))


def main():
	parser = argparse.ArgumentParser(description='PySysServices')
	parser.add_argument('--config', type=str, help='Path to the config file.')
	args = parser.parse_args()

	# Read config
	config = ReadConfig(args.config)
	globalConf = config['globalConf']
	globalConf['stateDir'] = os.path.abspath(globalConf['stateDir'])
	os.makedirs(globalConf['stateDir'], exist_ok=True)

	# logging
	logFile = Utils.ProcOptionalConfig(globalConf, 'logFile', None)
	SetupLogger(
		getattr(logging, globalConf['logLevel'], logging.INFO),
		logFile,
		sys.stdout)
	logger = logging.getLogger(__name__ + '.' + main.__name__)
	logger.info('PySysServices started.')

	# globalState & globalCache
	logger.info('Loading global states/caches...')
	globalCache = {}
	globalState = LoadGlobalState(globalConf)

	# sources
	globalCache['sources'] = LoadSources(config['sources'], globalConf)

	# Update Interval
	globalConf['updInterval'] = Utils.ProcOptionalConfig(
		globalConf, 'updInterval', 0.05)

	# initialize services
	svcStateDir = os.path.join(globalConf['stateDir'], 'services')
	os.makedirs(svcStateDir, exist_ok=True)
	gssFunc = FunctionMgr.GetFunction('GlobalStateSaver')
	gssService = gssFunc(svcStateDir, { 'repeat': True }, globalConf)
	services: List[ServiceFunction.ServiceFunction] = [
		gssService
	]

	services += InitServices(config['services'], globalConf, svcStateDir)

	swFunc = FunctionMgr.GetFunction('StatusWriter')
	swService = swFunc(svcStateDir, { 'repeat': True }, globalConf)
	services.append(swService)

	if globalConf['daemon']:
		wfiFunc = FunctionMgr.GetFunction('WaitForInterrupt')
		wfiService = wfiFunc(svcStateDir, { 'repeat': True }, globalConf)
		services.append(wfiService)

	swService.SetServices(services)

	# run services
	logger.info('Begin to run services...')
	for svc in services:
		svc.Start(globalState, globalCache)

	# terminate services
	logger.info('Terminating services...')
	for svc in services:
		svc.Shutdown()

	logger.info('PySysServices has been terminated.')


if __name__ == '__main__':
	exit(main())
